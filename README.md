# GDE Session

`gde-session` is a simple set of configuration files that "glue" all of the
other GDE components into a functional desktop.

## Dependencies

Since this package is just a few configuration files, it doesn't actually
depend on anything other than the build system (meson & ninja) at build time.
At runtime, however, GDE as a whole depends on quite a few packages and services.
This package seems like an appropriate place to list these dependencies, so here
they are:

- [gnome-session](https://gitlab.gnome.org/GNOME/gnome-session) (with systemd support)
- [gnome-settings-daemon](https://gitlab.gnome.org/GNOME/gnome-settings-daemon)
- [Wayfire](https://github.com/WayfireWM/wayfire)
- [wayfire-plugins-extra](https://github.com/WayfireWM/wayfire-plugins-extra)
- [wf-gsettings](https://github.com/DankBSD/wf-gsettings)
- [wayfire-plugin_dbus_interface](https://github.com/damianatorrpm/wayfire-plugin_dbus_interface)
- [xdg-desktop-portal-gtk](https://github.com/flatpak/xdg-desktop-portal-gtk)
- [xdg-desktop-portal-wlr](https://github.com/emersion/xdg-desktop-portal-wlr)
- [gde-panel](https://gitlab.com/carbonOS/gde/gde-panel)
- [gde-background](https://gitlab.com/carbonOS/gde/gde-background)
- [gde-dialogs](https://gitlab.com/carbonOS/gde/gde-dialogs)
- [gde-greeter](https://gitlab.com/carbonOS/gde/gde-greeter)
- [gde-gnome-bridge](https://gitlab.com/carbonOS/gde/gde-gnome-bridge)
- [squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard)

The following packages are temporary runtime dependencies, that will be
replaced once GDE re-implements their functionality or they otherwise
become unnecessary:

- [network-manager-applet](https://gitlab.gnome.org/GNOME/network-manager-applet)
- [grim](https://github.com/emersion/grim)
- [slurp](https://github.com/emersion/slurp)
- [wf-recorder](https://github.com/ammen99/wf-recorder)
- [adw-gtk3](https://github.com/lassekongo83/adw-gtk3)

The following packages provide the default assets that GDE uses, but they can be
replaced with alternative assets with minimal modification:

- Fonts
    - [Cantarell](https://gitlab.gnome.org/GNOME/cantarell-fonts): Primary UI font
    - [Source Code Pro](https://github.com/adobe-fonts/source-code-pro): Monospace font
    - [Twemoji](https://github.com/twitter/twemoji): Emojis
    - [Liberation](https://github.com/liberationfonts/liberation-fonts), 
      [Liberation Sans Narrow](https://github.com/liberationfonts/liberation-sans-narrow), 
      [Google crosextra](https://wiki.debian.org/SubstitutingCalibriAndCambriaFonts): MS font support
    - [Noto CJK](https://github.com/googlefonts/noto-cjk): Support for CJK characters
    - [Noto](https://github.com/googlefonts/noto-fonts): Default system font, fallback for all languages
    - [OpenDyslexic](https://opendyslexic.org/): Accessible font for Dyslexia
- Icons
    - [adwaita-icon-theme](https://gitlab.gnome.org/GNOME/adwaita-icon-theme)
    - [hicolor-icon-theme](https://gitlab.freedesktop.org/xdg/default-icon-theme)
- Sounds: [xdg-sound-theme](https://gitlab.freedesktop.org/xdg/xdg-sound-theme)
- Wallpapers
    - [gnome-backgrounds](https://gitlab.gnome.org/GNOME/gnome-backgrounds)
    - [elementary-wallpapers](https://github.com/elementary/wallpapers)
    (with [custom integration script](https://gitlab.com/carbonOS/build-meta/-/blob/main/files/elementary-gen-wp-list))

The following packages provide the list of default apps that GDE ships with:

- [nautilus](https://gitlab.gnome.org/GNOME/nautilus): File manager
- [sushi](https://gitlab.gnome.org/GNOME/sushi): Nautilus' previewer
- [epiphany](https://gitlab.gnome.org/GNOME/epiphany): Web Browser
- [gnome-text-editor](https://gitlab.gnome.org/GNOME/gnome-text-editor)
- [gnome-calculator](https://gitlab.gnome.org/GNOME/gnome-calculator)
- [seahorse](https://gitlab.gnome.org/GNOME/seahorse): Keyring Manager
- [gnome-contacts](https://gitlab.gnome.org/GNOME/gnome-contacts)
- [geary](https://gitlab.gnome.org/GNOME/geary): Email app
- [gnome-calendar](https://gitlab.gnome.org/GNOME/gnome-calendar)
- [eye-of-gnome](https://gitlab.gnome.org/GNOME/eog): Image viewer
- [evince](https://gitlab.gnome.org/GNOME/evince): Document viewer
- [gnome-clocks](https://gitlab.gnome.org/GNOME/gnome-clocks)
- [gnome-software](https://gitlab.gnome.org/GNOME/gnome-software)
- [gnome-weather](https://gitlab.gnome.org/GNOME/gnome-weather)
- [gnome-terminal](https://gitlab.gnome.org/GNOME/gnome-terminal)
- [gnome-system-monitor](https://gitlab.gnome.org/GNOME/gnome-system-monitor)
- [gnome-disk-utility](https://gitlab.gnome.org/GNOME/gnome-disk-utility)
- [gnome-logs](https://gitlab.gnome.org/GNOME/gnome-logs)
- [gnome-settings](https://gitlab.gnome.org/GNOME/gnome-control-center)
- [yelp](https://gitlab.gnome.org/GNOME/yelp): Help browser
- [baobab](https://gitlab.gnome.org/GNOME/baobab): Disk Usage Analyzer
- [gnome-characters](https://gitlab.gnome.org/GNOME/gnome-characters): Character map

## Building

Development is easiest on carbonOS. Simply switch carbonOS to the devel variant:

```bash
$ updatectl switch --base=devel
[Enter your credentials at the prompt]
[Reboot]
```

Then build and install the package:

```bash
$ meson _builddir --prefix=/your/prefix/here
[...]
$ ninja -C _builddir install
[...]
```

