#!/usr/bin/env bash
# This script prepares the gnome-session and systemd launch environments for Wayfire

# Tell gnome-session to put these variables into the environment
for var in WAYLAND_DISPLAY DISPLAY XAUTHORITY _JAVA_AWT_WM_NONREPARENTING; do
    systemd-cat -t gde-session echo "Sending $var to session manager"
    busctl --user call org.gnome.SessionManager /org/gnome/SessionManager org.gnome.SessionManager Setenv "ss" "$var" "${!var}"
done

# Tell systemd that Wayfire is ready
systemd-cat -t gde-session echo "Telling systemd to continue session startup"
systemd-notify --ready
